<?php

declare(strict_types=1);

use Paneric\PdoWrapper\PDOBuilder;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\QueryBuilder;
use Paneric\PdoWrapper\SequencePreparator;
use Paneric\PdoWrapper\DataPreparator;

$pdoWrapperConfig = [

    'limit' => 10,
    'host' => 'localhost',
    'dbName' => 'oauth_server',
    'charset' => 'utf8',
    'user' => 'root',
    'password' => 'root',
    'options' => [
        PDO::ATTR_PERSISTENT         => true,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //\PDO::ERRMODE_SILENT, \PDO::ERRMODE_WARNING, \PDO::ERRMODE_EXCEPTION
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::MYSQL_ATTR_FOUND_ROWS   => false, // true -> matched, false -> affected
    ],

];

$pdoBuilder = new PDOBuilder();

return new Manager(
    $pdoBuilder->build($pdoWrapperConfig),
    new QueryBuilder(new SequencePreparator()),
    new DataPreparator()
);
