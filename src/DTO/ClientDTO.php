<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\DTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class ClientDTO implements HydratorInterface, JsonSerializable
{
    private $id;
    private $clientId;
    private $clientSecret;
    private $credentialId;
    private $name;
    private $homepageUrl;
    private $description;
    private $redirectUrl;
    private $type;
    private $types = ['web_server', 'native', 'mobile', 'single_page'];
    private $createdAt;
    private $updatedAt;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function getClientSecret(): ?string
    {
        return $this->clientSecret;
    }

    public function getCredentialId(): ?string
    {
        return $this->credentialId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getHomepageUrl(): ?string
    {
        return $this->homepageUrl;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCallbackUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getTypes(): ?array
    {
        return $this->types;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }


    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }

        if (
            $name === 'client_id' ||
            $name === 'client_secret' ||
            $name === 'credential_id' ||
            $name === 'homepage_url' ||
            $name === 'redirect_url'
        ) {
            $this->$str = $value;
        }
    }


    public function hydrate(array $attributes): self
    {
        $attributes = array_filter($attributes);

        if (isset($attributes['id'])) {
            $this->id = $attributes['id'];
        }

        if (isset($attributes['client_id'])) {
            $this->clientId = $attributes['client_id'];
        }

        if (isset($attributes['client_secret'])) {
            $this->clientSecret = $attributes['client_secret'];
        }

        if (isset($attributes['credential_id'])) {
            $this->credentialId = $attributes['credential_id'];
        }

        if (isset($attributes['name'])) {
            $this->name = $attributes['name'];
        }

        if (isset($attributes['homepage_url'])) {
            $this->homepageUrl = $attributes['homepage_url'];
        }

        if (isset($attributes['description'])) {
            $this->description = $attributes['description'];
        }

        if (isset($attributes['redirect_url'])) {
            $this->redirectUrl = $attributes['redirect_url'];
        }

        if (isset($attributes['type'])) {
            $this->type = $attributes['type'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->clientId !== null) {
            $attributes['client_id'] = $this->clientId;
        }

        if ($this->clientSecret !== null) {
            $attributes['client_secret'] = $this->clientSecret;
        }

        if ($this->credentialId !== null) {
            $attributes['credential_id'] = $this->credentialId;
        }

        if ($this->name !== null) {
            $attributes['name'] = $this->name;
        }

        if ($this->homepageUrl !== null) {
            $attributes['homepage_url'] = $this->homepageUrl;
        }

        if ($this->description !== null) {
            $attributes['description'] = $this->description;
        }

        if ($this->redirectUrl !== null) {
            $attributes['redirect_url'] = $this->redirectUrl;
        }

        if ($this->type !== null) {
            $attributes['type'] = $this->type;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
