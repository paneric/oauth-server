<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\DTO;

class RequestAuthenticationDTO
{
    /*
     * use RouteParserInterface->relativeUrlFor()
     *
     * https://aaronparecki.com/oauth-2-simplified/#creating-an-app
     * https://authorization-server.com/auth?response_type=code&client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&scope=photos&state=1234zyx
     */
    private $responseType;
    private $clientId;
    private $redirectUrl;
    private $scope;
    private $state;


    public function getResponseType(): ?string
    {
        return $this->responseType;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function getCallbackUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function getScope(): ?string
    {
        return $this->scope;
    }

    public function getState(): ?string
    {
        return $this->state;
    }


    public function hydrate(array $attributes): self
    {
        $attributes = array_filter($attributes);

        if (isset($attributes['response_type'])) {
            $this->responseType = $attributes['response_type'];
        }

        if (isset($attributes['client_id'])) {
            $this->clientId = $attributes['client_id'];
        }

        if (isset($attributes['redirect_url'])) {
            $this->redirectUrl = $attributes['redirect_url'];
        }

        if (isset($attributes['scope'])) {
            $this->scope = $attributes['scope'];
        }

        if (isset($attributes['state'])) {
            $this->state = $attributes['state'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->responseType !== null) {
            $attributes['response_type'] = $this->responseType;
        }

        if ($this->clientId !== null) {
            $attributes['client_id'] = $this->clientId;
        }

        if ($this->redirectUrl !== null) {
            $attributes['redirect_url'] = $this->redirectUrl;
        }

        if ($this->scope !== null) {
            $attributes['scope'] = $this->scope;
        }

        if ($this->state !== null) {
            $attributes['state'] = $this->state;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}

/*
 $data = array(
  'dudu' => 'http://localhost:800/oauth/create',
    'foo' => 'bar',
    'baz' => 'boom',
    'cow' => 'milk',
    'php' => 'hypertext processor'
);

$dudu = http_build_query($data);

echo $dudu;

parse_str($dudu, $output);

var_dump($output);
 */
