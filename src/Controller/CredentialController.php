<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Controller;

use Paneric\CSRTriad\Controller\AppController;
use Paneric\OAUTHServer\Service\CredentialService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class CredentialController extends AppController
{
    protected $credentialService;
    protected $twig;


    public function __construct(CredentialService $credentialService, Twig $twig)
    {
        $this->credentialService = $credentialService;
        $this->twig = $twig;
    }


    public function showClients(Response $response): Response
    {
        return $this->render(
            $response,
            '@oauth_server/client/show_all.html.twig',
            $this->credentialService->getClients()
        );
    }

    public function showClient(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@oauth_server/client/show.html.twig',
            ['client' => $this->credentialService->getClient($id)]
        );
    }

    public function addClient(Request $request, Response $response): Response
    {
        $result = $this->credentialService->createClient($request);
        if ($result === null) {
            return $this->render(
                $response,
                '@oauth_server/client/add_message.html.twig',
                []
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/add.html.twig',
            $result
        );
    }

    public function editClient(Request $request, Response $response, int $id): Response
    {
        $result = $this->credentialService->updateClient($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/oauth-server/clients/show',
                200
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/edit.html.twig',
            $result
        );
    }

    public function deleteClient(Request $request, Response $response, int $id): Response
    {
        $result = $this->credentialService->deleteClient($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/oauth-server/clients/show',
                200
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/delete.html.twig',
            ['id' => $id]
        );
    }
}