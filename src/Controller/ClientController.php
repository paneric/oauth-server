<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Controller;

use Paneric\CSRTriad\Controller\AppController;
use Paneric\OAUTHServer\Service\ClientService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ClientController extends AppController
{
    protected $clientService;
    protected $twig;


    public function __construct(ClientService $clientService, Twig $twig)
    {
        $this->clientService = $clientService;
        $this->twig = $twig;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@oauth_server/client/show_all.html.twig',
            $this->clientService->getAll()
        );
    }

    public function showAllByCredential(Response $response): Response
    {
        return $this->render(
            $response,
            '@oauth_server/client/show_all_by_credential.html.twig',
            $this->clientService->getAllByCredential()
        );
    }


    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@oauth_server/client/show.html.twig',
            ['client' => $this->clientService->get($id)]
        );
    }

    public function showByCredential(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@oauth_server/client/show_by_credential.html.twig',
            ['client' => $this->clientService->getByCredential($id)]
        );
    }


    public function add(Request $request, Response $response): Response
    {
        $result = $this->clientService->create($request);
        if ($result === null) {
            return $this->render(
                $response,
                '@oauth_server/client/add_message.html.twig',
                []
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/add.html.twig',
            $result
        );
    }

    public function addByCredential(Request $request, Response $response): Response
    {
        $result = $this->clientService->create($request);
        if ($result === null) {
            return $this->render(
                $response,
                '@oauth_server/client/add_by_credential_message.html.twig',
                []
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/add_by_credential.html.twig',
            $result
        );
    }


    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->clientService->update($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/oauth-server/clients/show',
                200
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/edit.html.twig',
            $result
        );
    }

    public function editByCredential(Request $request, Response $response, int $id): Response
    {
        $result = $this->clientService->update($request, $id, true);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/oauth-server/credential/clients/show',
                200
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/edit_by_credential.html.twig',
            $result
        );
    }


    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->clientService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/oauth-server/clients/show',
                200
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/delete.html.twig',
            ['id' => $id]
        );
    }

    public function deleteByCredential(Request $request, Response $response, int $id): Response
    {
        $result = $this->clientService->deleteByCredential($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/oauth-server/credential/clients/show',
                200
            );
        }

        return $this->render(
            $response,
            '@oauth_server/client/delete_by_credential.html.twig',
            ['id' => $id]
        );
    }
}
