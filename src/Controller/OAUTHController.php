<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Controller;

use Paneric\OAUTHServer\Service\OAUTHService;
use Paneric\OAUTHServer\Service\JWTService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class OAUTHController
{
    protected $oauthService;
    protected $routeParser;


    public function __construct(OAUTHService $oauthService)
    {
        $this->oauthService = $oauthService;
    }

    public function authenticate(Request $request, Response $response, string $token = null): Response
    {
        $result = $this->oauthService->authenticate($request, $token);
        
        if ($result === 303) {
            $payload = $this->oauthService->getPayload();
            
            return $this->redirect(
                $response,
                '/authe/authenticate?' . http_build_query($payload),
                $result
            );
        }
        
        return $this->jsonResponse(
            $response,
            $this->oauthService->getPayload(),
            $result
        );
    }

    protected function redirect(Response $response, string $url, int $status = null): Response
    {
        $response = $response->withHeader('Location', (string)$url);

        if ($status !== null) {
            return $response->withStatus($status);
        }

        return $response;
    }

    protected function jsonResponse(
        Response $response,
        array $data = null,
        int $status = null,
        int $encodingOptions = 0)
    : Response {
        $response->getBody()->write(
            $json = json_encode($data, JSON_THROW_ON_ERROR | $encodingOptions, 512)
        );

        if ($json === false) {
            throw new RuntimeException(json_last_error_msg(), json_last_error());
        }

        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');

        if (isset($status)) {
            return $response->withStatus($status);
        }

        return $response;
    }
    
    protected function redirectJsonResponse(
        Response $response, 
        string $url, 
        int $status = null, 
        array $data = null, 
        int $encodingOptions = null
    ): Response {
        $response = $this->jsonResponse($response, $data, $status, $encodingOptions);
        
        return $this->redirect($response, $url, $status);
    }
}
