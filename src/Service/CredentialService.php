<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Service;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\OAUTHServer\Interfaces\ClientQueryInterface;
use Paneric\OAUTHServer\Interfaces\ClientRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class CredentialService
{
    protected $clientRepository;
    protected $clientQuery;
    protected $guard;

    public function __construct(
        ClientRepositoryInterface $clientRepository,
        ClientQueryInterface $clientQuery,
        SessionInterface $session,
        GuardInterface $guard
    ) {
        parent::__construct($session);

        $this->clientRepository = $clientRepository;
        $this->clientQuery = $clientQuery;
        $this->guard = $guard;
    }

    public function getClient(int $clientId): ?object
    {
        return $this->clientQuery->findClientById(
            $clientId,
            $this->session->getData('authentication')['id']
        );
    }


    public function getClients(): array
    {
        $credentialId = $this->session->getData('authentication')['id'];

        return $this->jsonSerializeObjects(
            $this->clientQuery->findClients($credentialId)
        );
    }

    public function updateClient(Request $request) // OK
    {
        $validation = [];

        $credentialId = $this->session->getData('authentication')['id'];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[ClientDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->clientQuery->updateClient(
                    $request->getParsedBody()['id'],
                    $credentialId
                );
            }
        }

        $this->session->setFlash(['page_title' => 'form_credential_clients_edit_title'], 'value');
        $this->session->setFlash(['relation_name' => 'credential'], 'value');

        return [
            'relation' => $this->credentialRepository->findOneById($credentialId),
            'clients' => $this->clientRepository->findAll(),
            'relation_clients' => $this->jsonSerializeObjectsById(
                $this->clientQuery->findClients($credentialId)
            ),
            'validation' => $validation
        ];
    }

    public function deleteClient(Request $request, int $clientId): ?array // Ok
    {
        if ($request->getMethod() === 'POST') {

            return $this->clientQuery->removeClient(
                $clientId,
                $this->session->getData('authentication')['id']
            );
        }

        $this->session->setFlash(['page_title' => 'form_client_credential_delete_title'], 'value');
        $this->session->setFlash(['credential_client_delete_warning'], 'warning');

        return [];
    }
}
