<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Service;

use Paneric\OAUTHServer\DTO\RequestAuthenticationDTO;
use Psr\Http\Message\ServerRequestInterface as Request;

class OAUTHService
{
    private $clientService;
    private $tokenToken;
    
    private $payload;

    public function __construct(ClientService $clientService, TokenService $tokenService)
    {
        $this->clientService = $clientService;
        $this->tokenService = $tokenService;
    }

    public function authenticate(Request $request, string $token = null): ?int
    {
        if ($request->getMethod() === 'GET' && $token === null) {

            $validation = $request->getAttribute('validation')[RequestAuthenticationDTO::class];

            parse_str($request->getUri()->getQuery(), $attributes);

            if (empty($validation)) {
                if (
                    $this->clientService->getByClientIdAndRedirectUrl(
                        $attributes['client_id'],
                        $attributes['redirect_url']
                    ) !== null
                ) {
                    $this->payload = [
                        'token' => $this->tokenService->encrypt(json_encode($attributes))
                    ];

                    return 303; // REDIRECTION: 303 See Other
                }
            }

            $this->payload = [
                'error' => 'invalid_query_params',
            ];

            return 400; // CLIENT ERROR: 400 Bad Request
        }

        return [];
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes