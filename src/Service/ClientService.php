<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Service;

use Paneric\CSRTriad\Service;
use Paneric\OAUTHServer\Interfaces\ClientRepositoryInterface;
use Paneric\OAUTHServer\DTO\ClientDTO;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class ClientService extends Service
{
    protected $clientRepository;
    protected $guard;

    public function __construct(
        ClientRepositoryInterface $clientRepository,
        SessionInterface $session,
        GuardInterface $guard
    ) {
        parent::__construct($session);

        $this->clientRepository = $clientRepository;
        $this->guard = $guard;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_clients_show_title'], 'value');

        return [
            'clients' => $this->jsonSerializeObjects($this->clientRepository->findAll())
        ];
    }

    public function getAllByCredential(): array
    {
        $this->session->setFlash(['page_title' => 'content_clients_show_title'], 'value');

        $credentialId = $this->session->getData('authentication')['id'];
        
        $clients = $this->clientRepository->findBy(
            ['credential_id' => $credentialId]
        );
        
        return [
            'clients' => $this->jsonSerializeObjects($clients)
        ];
    }


    public function get(int $id): ?object
    {
        return $this->clientRepository->findOneById($id);
    }

    public function getByCredential(int $id): ?object
    {
        $credentialId = $this->session->getData('authentication')['id'];

        return $this->clientRepository->findOneBy([
            'id' => $id,
            'credential_id' => $credentialId
        ]);
    }

    public function getByClientIdAndRedirectUrl(string $clientId, string $redirectUrl): ?object
    {
        $client = $this->clientRepository->findOneBy(['redirect_url' => $redirectUrl]);

        if (trim($client->getClientId()) === trim($clientId)) {
            return $client;
        }
        
        return null;
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $clientDTO = new ClientDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[ClientDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'name' => $attributes['name'],
                    'homepage_url' => $attributes['homepage_url'],
                    'callback_url' => $attributes['callback_url'],
                ];

                if ($this->clientRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    $keys = $this->guard->generatePublicPrivateKeysPair();
                    $attributes['client_id'] = $keys['public_key'];
                    $attributes['client_secret'] = $keys['private_key'];

                    $attributes['credential_id'] = $this->session->getData('authentication')['id'];

                    $this->session->setFlash(['db_client_add_warning'], 'warning');
                    $this->session->setFlash(
                        ['client_id' =>  $keys['public_key'], 'client_secret' => $keys['private_key']],
                        'value'
                    );

                    return $this->clientRepository->create($clientDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_client_add_error'], 'error');
            }
        }

        $attributes['types'] = $clientDTO->getTypes();

        return [
            'client' => $attributes,
            'validation' => $validation
        ];
    }


    public function update(Request $request, int $id, bool $byCredential = false): ?array
    {
        $validation = [];

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[ClientDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'name' => $attributes['name'],
                    'homepage_url' => $attributes['homepage_url'],
                    'callback_url' => $attributes['callback_url'],
                ];

                if ($this->clientRepository->findByEnhanced($uniqueAttributes, 'OR', $id) === null) {
                    $clientDTO = new ClientDTO();

                    if ($byCredential) {
                        return $this->clientRepository->updateByCredential(
                            $id,
                            $this->session->getData('authentication')['id'],
                            $clientDTO->hydrate($attributes)
                        );
                    }
                    
                    return $this->clientRepository->update(
                        $id,
                        $clientDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_client_edit_error'], 'error');
            }
        }

        $clientDTO = $this->clientRepository->findOneById($id);
        $attributes = $clientDTO->convert();
        $attributes['types'] = $clientDTO->getTypes();

        return [
            'client' => $attributes,
            'validation' => $validation
        ];
    }


    public function delete(Request $request, int $id): ?array
    {
        if ($request->getMethod() === 'POST') {
            return $this->clientRepository->remove($id);
        }

        $this->session->setFlash(['client_delete_warning'], 'warning');

        return [];
    }

    public function deleteByCredential(Request $request, int $id): ?array
    {
        if ($request->getMethod() === 'POST') {
            return $this->clientRepository->removeByCredential(
                $id,
                $this->session->getData('authentication')['id']
            );
        }

        $this->session->setFlash(['client_delete_warning'], 'warning');

        return [];
    }
}
