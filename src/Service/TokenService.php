<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Service;

use Paneric\Interfaces\Guard\GuardInterface;

class TokenService
{
    private $guard;
    
    public function __construct(GuardInterface $guard)
    {
        $this->guard = $guard;
    }
    
    public function encrypt(string $payload): string
    {
        return $this->guard->encrypt($payload);
    }
}
