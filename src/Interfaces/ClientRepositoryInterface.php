<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Interfaces;

use Paneric\Interfaces\Hydrator\HydratorInterface;

interface ClientRepositoryInterface
{
    public function findOneById(int $id): ?object;

    public function findOneBy(array $criteria);

    public function findAll();

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array;

    public function create(HydratorInterface $hydrator): ?array;

    public function update(int $id, HydratorInterface $hydrator): ?array;

    public function remove(int $id): ?array;
}
