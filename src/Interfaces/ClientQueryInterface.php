<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Interfaces;

interface ClientQueryInterface
{
    public function findCredential(int $clientId): ?object;

    public function setCredential(int $clientId, int $credentialId): ?array;
    public function removeCredential(int $clientId): ?array;
}
