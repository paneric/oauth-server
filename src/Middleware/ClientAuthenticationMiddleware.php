<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\Middleware;

use Paneric\OAUTHServer\DTO\RequestAuthenticationDTO;
use Paneric\OAUTHServer\Interfaces\ClientRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use InvalidArgumentException;

class ClientAuthenticationMiddleware implements MiddlewareInterface
{
    private $clientRepository;

    public function __construct(
        ClientRepositoryInterface $clientRepository
    ) {
        $this->clientRepository = $clientRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $requestAuthenticationDTO = new RequestAuthenticationDTO();
        $requestAuthenticationDTO->hydrate($request->getQueryParams());


        $queryParams = $request->getQueryParams();

        if (empty($queryParams) || !isset($queryParams)) {
            throw new InvalidArgumentException(
                'PANERIC: missing parameters !!!'
            );
        }


        return $handler->handle($request);
    }
}

//https://authorization-server.com/auth?response_type=code&
//  client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&scope=photos&state=1234zyx