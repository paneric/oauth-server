<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\PDO\Repository;

use Paneric\Interfaces\Hydrator\HydratorInterface;
use Paneric\OAUTHServer\Interfaces\ClientRepositoryInterface;
use Paneric\OAUTHServer\DTO\ClientDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class ClientRepository extends PDORepository implements ClientRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'clients';
        $this->dtoClass = ClientDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }

    public function updateByCredential(int $id, string $credentialId, HydratorInterface $hydrator): ?array
    {
        try {
            $this->manager->setTable($this->table);

            $this->manager->update(
                $hydrator->convert(), 
                ['id' => $id, 'credential_id' => $credentialId]
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
    
    public function removeByCredential(int $id, string $credentialId): ?array
    {
        try {
            $this->manager->setTable($this->table);

            $this->manager->delete([
                'id' => $id, 
                'credential_id' => $credentialId
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
