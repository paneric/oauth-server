<?php

declare(strict_types=1);

namespace Paneric\OAUTHServer\PDO\Query;

use Exception;
use Paneric\Authorization\DTO\AccessDTO;
use Paneric\OAUTHServer\Interfaces\ClientQueryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class ClientQuery implements ClientQueryInterface
{
    private $table = 'clients';

    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function findCredential(int $clientId): ?object
    {
        $this->manager->setTable($this->table);
        $this->manager->setDTOClass(CredentialDTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT at.* 
            FROM clients pt
            LEFT JOIN credentials at ON pt.credential_id = at.id
            WHERE pt.id = :id
        ';

        $stmt = $this->manager->setStmt($query, ['id' => $clientId]);

        $credential = $stmt->fetch();

        if ($credential === false){
            return null;
        }

        if ($credential->getId() === null){
            return null;
        }

        return $credential;
    }

    public function setCredential(int $clientId, int $credentialId): ?array
    {
        try {
            $this->manager->setTable($this->table);
            $this->manager->update(['credential_id' => $credentialId], ['id' => $clientId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function removeCredential(int $clientId): ?array
    {
        try {
            $this->manager->setTable($this->table);
            $this->manager->update(['credential_id' => null], ['id' => $clientId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
