<?php

declare(strict_types=1);

use DI\Container;
use Paneric\PdoWrapper\Manager;
use Paneric\OAUTHServer\PDO\Repository\ClientRepository;
use Paneric\OAUTHServer\Service\ClientService;
use Paneric\OAUTHServer\Service\OAUTHService;
use Paneric\OAUTHServer\Service\TokenService;
use Paneric\OAUTHServer\Controller\ClientController;
use Paneric\OAUTHServer\Controller\OAUTHController;
use Twig\Environment as Twig;
use Paneric\Session\SessionWrapper;
use Paneric\Interfaces\Guard\GuardInterface;

return [
    ClientRepository::class => static function (Container $container): ClientRepository
    {
        return new ClientRepository($container->get(Manager::class));
    },

    ClientService::class => static function (Container $container): ClientService
    {
        return new ClientService(
            $container->get(ClientRepository::class),
            $container->get(SessionWrapper::class),
            $container->get(GuardInterface::class)
        );
    },

    ClientController::class => static function (Container $container): ClientController
    {
        return new ClientController(
            $container->get(ClientService::class),
            $container->get(Twig::class)
        );
    },
    
    TokenService::class => static function (Container $container): TokenService
    {
        return new TokenService(
            $container->get(GuardInterface::class)            
        );
    },

    OAUTHService::class => static function (Container $container): OAUTHService
    {
        return new OAUTHService(
            $container->get(ClientService::class),
            $container->get(TokenService::class)
        );
    },
    
    OAUTHController::class => static function (Container $container): OAUTHController
    {
        return new OAUTHController(
            $container->get(OAUTHService::class)
        );
    },
];
