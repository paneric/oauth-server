<?php

return [
    /* common */

    'main' => 'Main page',
    'oauth-server' => 'OAUTH Server',
    'start' => 'Start',
    'client' => 'Client',
    'clients' => 'Clients',

    'name' => 'Application name',
    'description' => 'Application description',
    'homepage_url' => 'Application homepage url',
    'callback_url' => 'Application callback url',
    'type' => 'Application type',
    'id' => 'Client Id',
    'secret' => 'Client secret',

    'web_server' => 'Web-server app (confidential)',
    'native' => 'Native app (confidential)',
    'mobile' => 'Mobile app (public)',
    'single_page' => 'Single-page app (public)',

    'cancel' => 'Cancel',
    'add' => 'Add',
    'confirm' => 'Confirm',
    'modify' => 'Modify',
    'update' => 'Update',
    'delete' => 'Delete',
    'choose' => 'Choose...',
    'back' => 'Back',
    'got_it' => 'Got it',

    'client_id' => 'Client Id',
    'client_secret' => 'Client secret',
    'client_name_help' => 'Enter your application name',
    'client_description_help' => 'Enter short description of your application',
    'client_homepage_url_help' => 'Enter your application homepage url',
    'client_callback_url_help' => 'Enter your application callback url',
    'client_type_help' => 'Choose your application type',
    'client_id_help' => 'This is auto-generated client public identifier. Please store it in a secure place',
    'client_secret_help' => 'This is auto-generated client confidential identifier. Please store it in a secure place',

    /* oauth_server.client.add */

    'form_client_add_title' => 'Add new client',

    'db_client_add_error' => 'One or more (name, homepage url or callback url) values is/are already in use.',

    'message_client_add_title' => 'Security warining',

    'db_client_add_warning' => 'Please copy and store with the highest precautions both client id and client secret. There will be no way to retrieve client secret unless reset.',

    /* 'oauth_server.client.show' */

    'content_clients_show_title' => 'Clients',

    /* oauth_server.client.edit */

    'form_client_edit_title' => 'Modify client',

    'db_client_edit_error' => 'One or both (level or reference) values is/are already in use.',

    /* oauth_server.client.delete */

    'form_client_delete_title' => 'Delete client',

    'client_delete_warning' => 'Your are about delete a client. Please confirm.',
];