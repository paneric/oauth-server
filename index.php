<?php

declare(strict_types=1);

define('ENV', 'dev');

define('ROOT_FOLDER', __DIR__ . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require ROOT_FOLDER . 'vendor/autoload.php';

use Paneric\OAUTHServer\AppClient\AppClientService;
use Paneric\OAUTHServer\OAUTHServerController;

use Slim\Psr7\Factory\{UriFactory, ServerRequestFactory, ResponseFactory};

$guard = require 'init_guard.php';
$manager = require 'init_manager.php';
$sessionWrapper = require 'init_session_wrapper.php';
$twig = require 'init_twig_builder.php';

$requestMethod = $_SERVER['REQUEST_METHOD'];
$requestUri =  $_SERVER['REQUEST_URI'];

$uriFactory = new UriFactory();
$uri = $uriFactory->createUri($_SERVER['REQUEST_URI']);

$requestFactory = new ServerRequestFactory();
$request = $requestFactory->createServerRequest($_SERVER['REQUEST_METHOD'], $uri);

$responseFactory = new ResponseFactory();
$response = $responseFactory->createResponse();

$appClientService = new AppClientService();
$appClientController = new OAUTHServerController($twig, $sessionWrapper, $appClientService);

$response = $appClientController->index($request, $response);

echo $response->getBody();