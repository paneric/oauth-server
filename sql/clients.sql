SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `clients` (
                           `id` int(11) NOT NULL,
                           `client_id` text COLLATE utf8_unicode_ci NOT NULL,
                           `credential_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                           `client_secret` text COLLATE utf8_unicode_ci NOT NULL,
                           `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                           `homepage_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                           `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                           `callback_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                           `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                           `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                           `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `clients`
    ADD PRIMARY KEY (`id`),
    ADD KEY `credential_id_idx` (`credential_id`),
    ADD UNIQUE KEY `name_idx` (`name`),
    ADD KEY `homepage_url_idx` (`homepage_url`),
    ADD UNIQUE KEY `callback_url_idx` (`callback_url`),
    ADD KEY `type_idx` (`type`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `clients`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;