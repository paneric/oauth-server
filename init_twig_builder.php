<?php

declare(strict_types=1);

use Paneric\Twig\Extension\SessionExtension;

use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Bridge\Twig\Extension\TranslationExtension;

use Paneric\Twig\Extension\CSRFExtension;

use Paneric\Validation\ValidatorBuilder;
use Paneric\Twig\Extension\ValidationExtension;

use Twig\Extension\DebugExtension;
use Paneric\Twig\Extension\ArrayExtension;
use Paneric\Twig\Extension\StringExtension;
use Paneric\Twig\Extension\HtmlExtension;

use Paneric\Twig\TwigBuilder;

$translationConfig = require 'translation_en.php';

$translator = new Translator('en');
$translator->addLoader('array', new ArrayLoader());
$translator->addResource(
    'array',
    $translationConfig,
    'en'
);

$csrfConfig = [

    'csrf_key_name' => 'csrf_key',
    'csrf_key_length' => 32,
    'csrf_hash_name' => 'csrf_hash',

];

$csrfExtension = new CSRFExtension(
    $sessionWrapper,
    $guard,
    $csrfConfig
);

$validatorConfig = require 'validation.php';
$validatorBuilder = new ValidatorBuilder();
$validator = $validatorBuilder->build('en');

//return $validator;

$twigBuilderConfig = [

    'templates_dirs' => [
        'app_client' => ROOT_FOLDER . 'src/AppClient/templates/',
        'oauth_server' => ROOT_FOLDER . 'src/templates/',
    ],
    'options' => [
        'debug' => true, /* "prod" false */
        'charset' => 'UTF-8',
        'strict_variables' => false,
        'autoescape' => 'html',
        'cache' => false, /* "prod" ROOT_FOLDER.'/var/cache/twig'*/
        'auto_reload' => null,
        'optimizations' => -1,
    ],

];

$twigBuilder = new TwigBuilder();

$environment = $twigBuilder->build($twigBuilderConfig);

$sessionExtension = new SessionExtension(
    $sessionWrapper,
    $translator
);
$environment->addExtension($sessionExtension);

$translationExtension = new TranslationExtension($translator);
$environment->addExtension($translationExtension);

$environment->addExtension($csrfExtension);

$validationExtension = new ValidationExtension($validator);
$environment->addExtension($validationExtension);

$environment->addExtension(new DebugExtension());
$environment->addExtension(new ArrayExtension());
$environment->addExtension(new StringExtension());
$environment->addExtension(new HtmlExtension());

return $environment;
