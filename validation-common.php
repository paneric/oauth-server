<?php

declare(strict_types=1);

use Paneric\OAUTHServer\DTO\ClientDTO;
use Paneric\OAUTHServer\DTO\RequestAuthenticationDTO;

return [
    'validation' => [
        'oauth_server.client_by_credential.add' => [
            'methods' => ['POST'],
            ClientDTO::class => [
                'rules' => [
                    'name' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'type' => [
                        'required' => [],
                        'is_one_of' => ['web_server', 'native', 'mobile', 'single_page'],
                    ],
                    'description' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'homepage_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                    'redirect_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                ],
            ],
        ],
        'oauth_server.client_by_credential.edit' => [
            'methods' => ['POST'],
            ClientDTO::class => [
                'rules' => [
                    'name' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'type' => [
                        'required' => [],
                        'is_one_of' => ['web_server', 'native', 'mobile', 'single_page'],
                    ],
                    'description' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'homepage_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                    'redirect_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                ],
            ],
        ],
        'oauth_server.client_by_credential.delete' => [
            'methods' => ['POST'],
            ClientDTO::class => [
                'rules' => [],
            ],
        ],
        'oauth_server.oauth.authenticate' => [
            'methods' => ['GET'],
            RequestAuthenticationDTO::class => [
                'rules' => [
                    'response_type' => [
                        'required' => [],
                        'is_one_of' => ['code'],
                    ],
                    'client_id' => [
                        'required' => [],
                        'is_open_ssl_key' => [],
                    ],
                    'redirect_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                    'scope' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'state' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                ],
            ],
        ],
    ],
];
