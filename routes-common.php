<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\OAUTHServer\Controller\ClientController;
use Paneric\OAUTHServer\Controller\OAUTHController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;

$app->get('/oauth-server/credential/clients/show', function (Request $request, Response $response) {
    return $this->get(ClientController::class)->showAllByCredential($response);
})->setName('oauth_server.clients_by_credential.show');

$app->get('/oauth-server/credential/client/{id}/show', function (Request $request, Response $response, array $args) {
    return $this->get(ClientController::class)->showByCredential($response, (int) $args['id']);
})->setName('oauth_server.client_by_credential.show');

$app->map(['GET', 'POST'], '/oauth-server/credential/client/add', function (Request $request, Response $response) {
    return $this->get(ClientController::class)->addByCredential($request, $response);
})->setName('oauth_server.client_by_credential.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/oauth-server/credential/client/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(ClientController::class)->editByCredential($request, $response, (int) $args['id']);
})->setName('oauth_server.client_by_credential.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/oauth-server/credential/client/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(ClientController::class)->deleteByCredential($request, $response, (int) $args['id']);
})->setName('oauth_server.client_by_credential.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));
