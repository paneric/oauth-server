<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\OAUTHServer\Controller\OAUTHController;
use Paneric\Validation\ValidationMiddleware;

// route name needed by Authorization:
$app->get('/oauth-server/authenticate[/{token}]', function(
    Request $request,
    Response $response,
    array $args
) {
    return $this->get(OAUTHController::class)->authenticate($request, $response, $args['token']);
})->setName('oauth_server.oauth.authenticate')
    ->addMiddleware($container->get(ValidationMiddleware::class));
