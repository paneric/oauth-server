<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\OAUTHServer\Controller\ClientController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;

$app->get('/oauth-server/clients/show', function (Request $request, Response $response) {
    return $this->get(ClientController::class)->showAll($response);
})->setName('oauth_server.clients.show');

$app->get('/oauth-server/client/{id}/show', function (Request $request, Response $response, array $args) {
    return $this->get(ClientController::class)->show($response, (int) $args['id']);
})->setName('oauth_server.client.show');

$app->map(['GET', 'POST'], '/oauth-server/client/add', function (Request $request, Response $response) {
    return $this->get(ClientController::class)->add($request, $response);
})->setName('oauth_server.client.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/oauth-server/client/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(ClientController::class)->edit($request, $response, (int) $args['id']);
})->setName('oauth_server.client.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/oauth-server/client/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(ClientController::class)->delete($request, $response, (int) $args['id']);
})->setName('oauth_server.client.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));