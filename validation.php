<?php

declare(strict_types=1);

use Paneric\OAUTHServer\DTO\ClientDTO;

return [
    'validation' => [
        'oauth_server.client.add' => [
            'methods' => ['POST'],
            ClientDTO::class => [
                'rules' => [
                    'name' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'type' => [
                        'required' => [],
                        'is_one_of' => ['web_server', 'native', 'mobile', 'single_page'],
                    ],
                    'description' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'homepage_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                    'callback_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                ],
            ],
        ],
        'oauth_server.client.edit' => [
            'methods' => ['POST'],
            ClientDTO::class => [
                'rules' => [
                    'name' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'type' => [
                        'required' => [],
                        'is_one_of' => ['web_server', 'native', 'mobile', 'single_page'],
                    ],
                    'description' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'homepage_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                    'callback_url' => [
                        'required' => [],
                        'is_url' => [],
                    ],
                ],
            ],
        ],
        'oauth_server.client.delete' => [
            'methods' => ['POST'],
            ClientDTO::class => [
                'rules' => [],
            ],
        ],
    ],
];
